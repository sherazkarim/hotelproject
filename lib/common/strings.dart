import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class Strings {
  /**
   * Global Strings
   */
  static const String APP_NAME = "SAFARMINA";
  static const String FONT_FAMILY_HEADING = "Rubik";
  static const String FONT_FAMILY_TEXT = "Artegra";

  /**
   * screen titles
   */
  static const String TITLE_SPLASH = "Splash";
  static const String TITLE_CHECKIN_DASHBOARD = "Check In";
  static const String TITLE_CHECKIN_DETAILS = "Check In Details";
  static const String TITLE_NEW_CUSTOMER = "New Customer";
  static const String TITLE_DASHBOARD = "Dashboard";
  static const String TITLE_CHEKOUT = "CheckOut";
  static const String TITLE_RESERVATIONS = "Reservation";
  static const String TITLE_RESERVATION_FORM = "Reservation Form";
  static const String TITLE_SERVICE_ORDER = "Service Order";
  static const String TITLE_BILLING = "Billing";
  static const String TITLE_SERVICES = "Services";

  /**
   * Asset URI
   * images
   */
  static const String IMG_LOGO = "assets/img/g4.png";
  static const String IMG_BG = "assets/img/g307.png";

  /**
   *Text
   */
  static const String TEXT_BILLING_ENTRY = "Billing Entry";
  static const String TEXT_ROOM_SERVICES = "Room Services";
  static const String TEXT_FOOD_SERVICES = "Food Services";
  static const String TEXT_FINALIZE = "Finalize";
  static const String TEXT_ALL= "All";
  static const String TEXT_PENDING= "Pending";
  static const String TEXT_ROOMS= "Rooms";
  static const String TEXT_LOGIN = "LOGIN";
  static const String TEXT_SEND = "Send";
  static const String TEXT_NEXT = "Next";
  static const String TEXT_SAVE_DETAILS = "Save Details";
  static const String TEXT_CANCEL = "Cancel";
  static const String TEXT_COMPLETE_CHECK_OUT = "Complete CheckOut";
  static const String TEXT_RESET_SUCCESS_MSG = "Please check your mail to reset your account password";
  static const String TEXT_RESET_MSG = "Enter your email to reset your account password";
  static const String TEXT_HINT_MSG = "abc@gmail.com";
  static const String TEXT_CUSTOMER_TYPE = "Choose Customer Type";
  static const String TEXT_SELECT_CUSTOMER = "Select Customer";
  static const String TEXT_LOADING = "Loading...";
  static const String TEXT_CHOOSE_ROOM = "Choose Room";
  static const String TEXT_CHECK_IN = "CheckIn";
  static const String TEXT_CHECK_OUT = "CheckOut";
  static const String TEXT_RESERVATIONS = "Reservation";
  static const String TEXT_REPORTS = "Report";
  static const String TEXT_SERVICE_ORDER = "Service Order";
  static const String TEXT_BILLING = "Billing";
  static const String TEXT_SETTING = "Setting";
  static const String TEXT_BILLED_TO = "Billed To";
  static const String TEXT_CHECK_OUT_TIME = "Check Out Time";
  static const String TEXT_SERIAL = "Serial#";
  static const String TEXT_CHECK_IN_DATE = "Check In Date";
  static const String TEXT_CHECK_IN_TIME = "Check In Time";
  static const String TEXT_CHECK_OUT_DATE = "Check Out Date";
  static const String TEXT_FARE_DAY= "Fare/Day";
  static const String TEXT_DURATION= "Duration";
  static const String TEXT_SUB_TOTAL= "Subtotal";
  static const String TEXT_TOTAL= "Total";
  static const String TEXT_ITEM= "Item";
  static const String TEXT_PRICE= "Price";
  static const String TEXT_QUANTITY= "Quantity";
  static const String TEXT_DISOUNT= "Discount";
  static const String TEXT_ROOM_CHARGES= "Room Charges (A)";
  static const String TEXT_FOOD_CHARGES= "Food Charges (B)";
  static const String TEXT_TAX= "Tax (C)";
  static const String TEXT_DEPOSIT= "Deposit (D)";
  static const String TEXT_SUB_TOTAL_ABC= "Subtotal (A+B+C)";
  static const String TEXT_TOTAL_ABCD= "Total (A+B+C-D)";
  static const String TEXT_TAX_TYPE="Tax Type";
  static const String TEXT_PAYMENT_MODE="Payment Mode";
  static const String TEXT_ADDITIONAL_NOTES='Additional Notes...';
  static const String TEXT_DATE='Date';
  static const String TEXT_NAME="Name";
  static const String TEXT_ADULTS="Adults";
  static const String TEXT_FEMALES="Females";
  static const String TEXT_RESERVED="RESERVED";
  static const String TEXT_ID="ID";
  static const String TEXT_PAYEE="Payee";
  static const String TEXT_FOOD_CHARGE="Food Charges";
  static const String TEXT_ROOM_CHARGE="Room Charges";
  static const String TEXT_TAX_APPLIED="Tax Applied";
  static const String TEXT_STATUS="Status";
  static const String TEXT_PRINT="Print";
  static const String TEXT_SERVICES="Services";
  static const String TEXT_LOGOUT="Logout";



  /**
   * Error Messages
   */
  static const String ERROR_EMPTY = "This field can\'t be empty";
  static const String ERROR_FORGET_PASSWORD = "Forgot your password?";


/**
 * Colors
 */
  static const Color PRIMARY_COLOR = Color(0xFF44b2c9);
  static const Color SECONDARY_COLOR = Color(0xFF577482);
  static const Color GREY_COLOR = Color(0xFF939ea5);

/**
 * Tabbar
 */

}
