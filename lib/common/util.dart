class Util{
  static const String TAG = "safarmina";

  static log(String ref, String message){
    print("[${Util.TAG}][${ref}]: ${message}");
  }
}