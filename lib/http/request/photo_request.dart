import 'dart:convert';

import 'package:hotel_project/http/request/request.dart';
import 'package:hotel_project/models/photo.dart';
import 'package:http/http.dart' as http;

Future<Photo> getPhoto(String id) async {
  print("requesting image ");
  var url = Uri.https(Request.domain, "${Request.photo}/${id}");

  final response = await http.get(url);

  if (response.statusCode == 200) {
    return new Photo.fromJSON(jsonDecode(response.body));
  } else {
    throw Exception("Exception Occured!");
  }
}
