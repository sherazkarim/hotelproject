import 'dart:convert';

import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/http/request/request.dart';
import 'package:hotel_project/http/response/user_response.dart';
import 'package:hotel_project/models/user.dart';
import 'package:http/http.dart' as http;

Future<UserListResponse> getUserList() async {
  Util.log('get_user_list','requesting for data!');
  var uri = Uri.https(Request.domain, Request.userlist);

  final response = await http.get(uri);

  if (response.statusCode == 200) {
    return UserListResponse.fromJSON(response.body);
  } else {
    throw Exception("Network Error!");
  }
}

Future<User> getUser(String id) async {
  print('requesting for single user data!');
  var uri = Uri.https(Request.domain, "${Request.userlist}/${id}");

  final response = await http.get(uri);

  if (response.statusCode == 200) {
    return User.fromJson(jsonDecode(response.body));
  } else {
    throw Exception("Network Error!");
  }
}
