import 'dart:convert';

import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/http/request/request.dart';
import 'package:hotel_project/http/response/customer_response.dart';
import 'package:hotel_project/http/response/user_response.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/models/user.dart';
import 'package:http/http.dart' as http;

Future<CustomerListResponse> getCustomerList() async {
  Util.log('getCustomerList()', 'requesting for customer data!');
  var uri = Uri.http(Request.domain, Request.customerList);

  final response = await http.get(uri);

  if (response.statusCode == 200) {
    return CustomerListResponse.fromJSON(response.body);
  } else {
    Util.log('getCustomerList()', "Error! ${response.statusCode}");
    throw Exception("Network Error!");
  }
}

//TODO chanage url later
Future<Customer> getCustomer(String id) async {
  print('requesting for single user data!');
  var uri = Uri.http(Request.domain, "${Request.userlist}/${id}");

  final response = await http.get(uri);

  if (response.statusCode == 200) {
    return Customer.fromJson(jsonDecode(response.body));
  } else {
    throw Exception("Network Error!");
  }
}
