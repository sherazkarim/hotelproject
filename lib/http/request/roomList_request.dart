import 'dart:convert';
import 'dart:io';

import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/http/request/request.dart';
import 'package:hotel_project/http/response/customer_response.dart';
import 'package:hotel_project/http/response/room_list_response.dart';
import 'package:hotel_project/http/response/user_response.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/models/room.dart';
import 'package:http/http.dart' as http;

Future<RoomListResponse> getRoomList() async {
  Util.log('get_room_list','requesting for data!');
  var uri = Uri.http(Request.domain, Request.roomList);


  try {
    final response = await http.get(uri);
    Util.log("get_room_list_exception", "${uri}");


    if (response.statusCode == 200) {
      return RoomListResponse.fromJSON(response.body);
    } else {
      Util.log("get_room_list_exception", "${response.statusCode}");
      throw Exception("Network Error!");
    }

  } on Exception {
    throw Exception('No Internet connection');
  }


}

class FetchDataException {
}


//TODO change url later
Future<Room> getRoom(String id) async {
  print('requesting for single user data!');
  var uri = Uri.http(Request.domain, "${Request.userlist}/${id}");

  final response = await http.get(uri);

  if (response.statusCode == 200) {
    return Room.fromJson(jsonDecode(response.body));
  } else {
    throw Exception("Network Error!");
  }
}
