import 'dart:convert';

import 'package:hotel_project/models/reservation.dart';
import 'package:hotel_project/models/customer.dart';

class CheckInRequest {
  final Customer customer;
  final Reservation reservation;

  CheckInRequest(this.customer, this.reservation);

  factory CheckInRequest.fromJson(Map<String, dynamic> json) {
    var customer = Customer.fromJson(json['customer']);
    var reservation = Reservation.fromJson(json['reservation']);

    return CheckInRequest(customer, reservation);
  }

  Map<String, dynamic> toJson() => {
        "customer": customer,
        "reservation": reservation
      };
}
