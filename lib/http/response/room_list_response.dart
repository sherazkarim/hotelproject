import 'dart:convert';

import 'package:hotel_project/models/room.dart';

class RoomListResponse {
  final List<Room> roomList;

  RoomListResponse({required this.roomList});

  factory RoomListResponse.fromJSON(String json) {
    print('json list${json}');

    var jsonObj = jsonDecode(json) as List;
    List<Room> roomList = jsonObj.map((item) => Room.fromJson(item)).toList();

    return RoomListResponse(roomList: roomList);
  }
}
