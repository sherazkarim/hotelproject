import 'dart:convert';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/models/customer.dart';
class CustomerListResponse {
  final List<Customer> customerList;

  CustomerListResponse({required this.customerList});

  factory CustomerListResponse.fromJSON(String json) {
    Util.log('CustomerFromJSON ','json list ${json}');

    var jsonObj = jsonDecode(json) as List;
    List<Customer> customerList = jsonObj.map((item) => Customer.fromJson(item)).toList();

    return new CustomerListResponse(customerList: customerList);
  }
}
