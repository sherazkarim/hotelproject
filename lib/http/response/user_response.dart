import 'dart:convert';
import 'package:hotel_project/models/user.dart';

class UserListResponse {
  final List<User> userList;

  UserListResponse({required this.userList});

  factory UserListResponse.fromJSON(String json) {
    print('json list ${json}');

    var jsonObj = jsonDecode(json) as List;
    List<User> userlist = jsonObj.map((item) => User.fromJson(item)).toList();

    return new UserListResponse(userList: userlist);
  }
}
