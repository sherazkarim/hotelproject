import 'package:hotel_project/models/user.dart';

import 'customer.dart';

class Reservation {


  static const int STATUS_NEW = 0;
  static const int STATUS_CONFIRMED = 1;
  static const int STATUS_CANCELED = 2;
  static const int STATUS_REJECTED = 3;

  late final int id;
  late final Customer customer;
  late final int customerId;
  late final String date;
  late final String time;
  late final int durationOfStaysDays;
  late final int durationOfStaysHours;
  late final int adults;
  late final int children;
  late final double deposit;
  late final String vehicleNo;
  late final String reference;
  late final String remarks;
  late final int status;

  Reservation({
    required this.id,
    required this.customer,
    required this.customerId,
    required this.date,
    required this.time,
    required this.durationOfStaysDays,
    required this.durationOfStaysHours,
    required this.adults,
    required this.children,
    required this.deposit,
    required this.vehicleNo,
    required this.reference,
    required this.remarks,
    required this.status,
  });

  factory Reservation.fromJson(Map<String, dynamic> json) => Reservation(
        id: json['id'],
        customer: Customer.fromJson(json['customer']),
        customerId: json['customer_id'],
        date: json['date'],
        time: json['time'],
        durationOfStaysDays: json['duration_days'],
        durationOfStaysHours: json['duration_hours'],
        adults: json['adults'],
        children: json['children'],
        deposit: json['deposit'],
        vehicleNo: json['vehicleNo'],
        reference: json['reference'],
        remarks: json['remarks'],
        status: json['status'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'customer': customer,
        'customer_id': customerId,
        'date': date,
        'time': time,
        'duration_days': durationOfStaysDays,
        'duration_hours': durationOfStaysHours,
        'adults': adults,
        'children': children,
        'deposit': deposit,
        'vehicleNo': vehicleNo,
        'reference': reference,
        'remarks': remarks,
        'status': status,
      };

  String getStatusText() {

    switch (status) {
      case STATUS_NEW:
        return "New";
      case STATUS_CONFIRMED:
        return "Confirmed";
      case STATUS_CANCELED:
        return "Canceled";
      case STATUS_REJECTED:
        return "Rejected";
      default:
        return "n/a";
    }
  }

}
