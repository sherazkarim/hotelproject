class Photo {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photo(this.albumId, this.id, this.title, this.url, this.thumbnailUrl);

  factory Photo.fromJSON(Map<String, dynamic> jsonObj){
    return Photo(jsonObj['albumId'], jsonObj['id'], jsonObj['title'], jsonObj['url'], jsonObj['thumbnailUrl']);
  }
}
