import 'package:flutter/material.dart';

class Room {
  late final int roomId;
  late final String roomCode;
  late final String floor;
  late final String roomType;
  late final String capacity;
  late final String rent;
  late final String description;
  late final String services;
  late final int status;

  static const int STATUS_ALL = -1;
  static const int STATUS_VACANT = 0;
  static const int STATUS_CHEKED_IN = 1;
  static const int STATUS_RESERVED = 2;

  Room({
    required this.roomId,
    required this.floor,
    required this.roomCode,
    required this.roomType,
    required this.capacity,
    required this.rent,
    required this.description,
    required this.services,
    required this.status,
  });

  factory Room.fromJson(Map<String, dynamic> json) => Room(
        roomId: json['id'],
        roomCode: json['roomCode'],
        floor: json['floor'],
        roomType: json['roomType'],
        capacity: json['capacity'],
        rent: json['rent'],
        description: json['description'],
        services: json['services'],
        status: json['status'],
      );

  Map<String, dynamic> toJson() => {
        'roomId': roomId,
        'floor': floor,
        'roomType': roomType,
        'capacity': capacity,
        'rent': rent,
        'description': description,
        'services': services,
        'status': status,
      };

  getRoomStatusIcon() {
    switch (status) {
      case Room.STATUS_VACANT:
        return Icon(
          Icons.radio_button_unchecked_sharp,
          size: 50,
          color: Color(0xFF02bbd7),
        );
      case Room.STATUS_CHEKED_IN:
        return Icon(
          Icons.check_outlined,
          size: 50,
          color: Color(0xFF02bbd7),
        );
      case Room.STATUS_RESERVED:
        return Icon(
          Icons.arrow_right_alt_outlined,
          size: 50,
          color: Color(0xFF02bbd7),
        );
    }
  }
}
