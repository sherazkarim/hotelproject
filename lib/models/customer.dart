import 'package:hotel_project/models/user.dart';

class Customer extends User {
  late final String cnic;
  late final String address;

  Customer(
      {required id,
      required name,
      required username,
      required email,
      required this.cnic,
      required this.address})
      : super(id: id, name: name, username: username, email: email);

  Customer.create(
      {required id,
        required name,
        required email,
        required this.cnic,
        required this.address})
      : super(id: id, name: name, username: "", email: email);

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
      id: json['id'],
      name: json['name'],
      username: json['username'],
      email: json['email'],
      cnic: json['cnic'],
      address: json['address']);

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'username': username,
        'email': email,
        'cnic': cnic,
        'address': address
      };
}
