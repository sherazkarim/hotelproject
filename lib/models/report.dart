import 'package:flutter/material.dart';

class Report{
  late int status;

  Report({required this.status});

  static const int REPORT_DAILY = -1;
  static const int REPORT_WEEKLY = 0;
  static const int REPORT_MONTHLY = 1;

  getReportTitle() {
    switch (status) {
      case Report.REPORT_DAILY:
        return Text("Daily");
      case Report.REPORT_WEEKLY:
        return Text("Weekly");
      case REPORT_MONTHLY:
        return Text("Monthly");
    }
  }
}
