class User {
  static const String LABEL_ID = "Id";
  static const String LABEL_NAME = "Name";
  static const String LABEL_USERNAME = "Username";
  static const String LABEL_PASSWORD = "Password";
  static const String LABEL_EMAIL = "Email";
  static const String LABEL_NOTE = "Notes";
  static const String LABEL_STATUS = "Status";

  static const String LABEL_DATE = "Enter Date";
  static const String LABEL_TIME = "Enter Time";
  static const String LABEL_STAY = "Duration of Stay(Days)";
  static const String LABEL_HOURS = "Duration of Stay(Hours)";
  static const String LABEL_ADULTS = "Duration of Stay(Hours)";
  static const String LABEL_CHILDRENS = "Children";
  static const String LABEL_DEPOSIT = "Deposit";
  static const String LABEL_VEHICAL_NO = "Vehical No";
  static const String LABEL_REFERENCE = "Reference";

  static const String LABEL_CNIC = "CNIC";
  static const String LABEL_PHONE = "Phone";
  static const String LABEL_ADDRESS = "Address";

  static const String LABEL_CHECK_OUT_DATE = "CheckOut Date";
  static const String LABEL_CITY = "City";
  static const String LABEL_NUMBER = "Phone Number";
  static const String LABEL_POSTAL_CODE = "postal Code";
  static const String LABEL_PAYEE_STATE = "State";
  static const String LABEL_PAYEE_COUNTRY = "Country";

  static const String LABEL_TAX_AMOUNT = "Tax Amount";
  static const String LABEL_AMOUNT_PAID = "Amount Paid";
  static const String LABEL_CODE = "Code";

  final int id;
  final String name;
  final String username;
  final String email;
  late String note;
  late String status;

  User(
      {required this.id,
      required this.name,
      required this.username,
      required this.email});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'],
        name: json['name'],
        username: json['username'],
        email: json['email']);
  }

  Map<String, dynamic> toJson() =>
      {'id': id, 'name': name, 'username': username, 'email': email};
}
