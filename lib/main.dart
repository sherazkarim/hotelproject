import 'package:flutter/material.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/screens/auth/login_screen.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

// @dart=2.9
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: Strings.TITLE_SPLASH,
      debugShowCheckedModeBanner: false,
      home:Splash()
    );
  }

}

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Util.log("splash", "build");

    
    return SplashScreenView(
      navigateRoute: LoginScreen(),
      duration: 3000,
      imageSize: 130,
      imageSrc: Strings.IMG_LOGO,
      backgroundColor: Colors.white,
      text: Strings.APP_NAME,
      textStyle: TextStyle(
        color: Colors.white,
        fontSize: 24,
        fontFamily: Strings.FONT_FAMILY_HEADING
      ),
    );
  }
}

