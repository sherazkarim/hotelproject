import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/screens/checkIn/details.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/common/strings.dart';

class NewCustomerDetails extends StatefulWidget {
  const NewCustomerDetails({Key? key}) : super(key: key);

  @override
  _NewCustomerDetailsState createState() => _NewCustomerDetailsState();
}

class _NewCustomerDetailsState extends State<NewCustomerDetails> {
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController cnic = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController address = TextEditingController();

  final _formkey = GlobalKey<FormState>();

  _doAddNewCustomer(){

    Util.log('Button', 'Next');
    Util.log('name', '${name.text}');
    Util.log('phone', '${phone.text}');
    Util.log('cnic', '${cnic.text}');

    Customer customer = Customer.create(
        id: -1,
        name: name.text,
        email: email.text,
        cnic: cnic.text,
        address: address.text);
    String json = jsonEncode(customer);
    print(json);

    //todo send post request to server!
    if (_formkey.currentState!.validate()) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                CheckInDetails(customer: customer)),
      );
    }


  }

  @override
  Widget build(BuildContext context) {

    Util.log("check_in_new_customer", "init state");

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TITLE_NEW_CUSTOMER),
        backgroundColor: Color(0xFF02bbd7),
      ),
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: name,
                          decoration: InputDecoration(
                              labelText: User.LABEL_NAME,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: cnic,
                          decoration: InputDecoration(
                              labelText: User.LABEL_CNIC,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: phone,
                          decoration: InputDecoration(
                              labelText: User.LABEL_PHONE,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: email,
                          decoration: InputDecoration(
                              labelText: User.LABEL_EMAIL,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) { //todo email validate
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: address,
                          decoration: InputDecoration(
                              labelText: User.LABEL_ADDRESS,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: ElevatedButton(
                            onPressed: () {
                              //save
                              _doAddNewCustomer();
                            },
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Color(0xFF939ea5)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        side: BorderSide(color: Colors.grey)))),
                            child: Text(
                              Strings.TEXT_NEXT,
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: Strings.FONT_FAMILY_TEXT,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
