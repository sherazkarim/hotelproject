import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/http/request/checkIn_request.dart';
import 'package:hotel_project/http/request/roomlist_request.dart';
import 'package:hotel_project/http/response/room_list_response.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/models/reservation.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/models/room.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:intl/intl.dart';

class CheckInDetails extends StatefulWidget {
  final Customer customer;

  const CheckInDetails({Key? key, required this.customer}) : super(key: key);

  @override
  _CheckInDetailsState createState() => _CheckInDetailsState();
}

class _CheckInDetailsState extends State<CheckInDetails> {
  late Future<RoomListResponse> roomList;

  Room? selectedRoom;

  final _formkey = GlobalKey<FormState>();
  TextEditingController dateinput = TextEditingController();
  TextEditingController timeinput = TextEditingController();
  TextEditingController durationOfStaysDays = TextEditingController();
  TextEditingController durationOfStaysHours = TextEditingController();
  TextEditingController adults = TextEditingController();
  TextEditingController children = TextEditingController();
  TextEditingController deposite = TextEditingController();
  TextEditingController vehicleNo = TextEditingController();
  TextEditingController reference = TextEditingController();
  TimeOfDay selectedTime = TimeOfDay.now();

  @override
  void initState() {
    dateinput.text = ""; //set the initial value of text field
    super.initState();
    roomList = getRoomList();
  }

  String? room;
  String defalutfloor = "1st Floor";
  List<String> roomfloor = [
    'Ground Floor',
    '1st Floor',
    '2nd Floor',
    '3rd Floor',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TITLE_CHECKIN_DETAILS),
        backgroundColor: Color(0xFF02bbd7),
      ),
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            Strings.TEXT_CHOOSE_ROOM,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Strings.FONT_FAMILY_HEADING),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: FutureBuilder<RoomListResponse>(
                                  future: roomList,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      if (selectedRoom == null)
                                        selectedRoom = snapshot.data?.roomList
                                            .elementAt(0);

                                      return DropdownButtonHideUnderline(
                                        child: DropdownButton2(
                                          items: snapshot.data?.roomList
                                              .map((item) => DropdownMenuItem<
                                                      Room>(
                                                  value: item,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 10.0),
                                                    child: Text(
                                                      item.roomCode,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontFamily: Strings
                                                              .FONT_FAMILY_TEXT),
                                                    ),
                                                  )))
                                              .toList(),
                                          value: selectedRoom,
                                          onChanged: (username) {
                                            setState(() {
                                              selectedRoom = username as Room;
                                              Util.log('Selected Room',
                                                  '${selectedRoom}');
                                            });
                                          },
                                          buttonHeight: 10,
                                          buttonWidth: 110,
                                          itemHeight: 40,
                                        ),
                                      );
                                    } else {
                                      return Text(Strings.TEXT_LOADING);
                                    }
                                  }),
                            ),
                          )
                        ],
                      ),
                      Container(
                          child: TextFormField(
                        controller: dateinput,
                        decoration: InputDecoration(
                            suffixIcon: Icon(Icons.calendar_today),
                            // icon: Icon(Icons.calendar_today),
                            labelText: User.LABEL_DATE),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return Strings.ERROR_EMPTY;
                          }
                          return null;
                        },
                        readOnly: true,
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            print(
                                pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                            String formattedDate =
                                DateFormat('yyyy-MM-dd').format(pickedDate);
                            print(
                                formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                            setState(() {
                              dateinput.text =
                                  formattedDate; //set output date to TextField value.
                            });
                          } else {
                            Strings.ERROR_EMPTY;
                          }
                        },
                      )),
                      Container(
                          alignment: Alignment.center,
                          child: Center(
                              child: TextFormField(
                            controller: timeinput,
                            //editing controller of this TextField
                            decoration: InputDecoration(
                                suffixIcon: Icon(Icons.timer),
                                labelText: User.LABEL_TIME //label text of field
                                ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return Strings.ERROR_EMPTY;
                              }
                              return null;
                            },
                            readOnly: true,
                            //set it true, so that user will not able to edit text
                            onTap: () async {
                              TimeOfDay? pickedTime = await showTimePicker(
                                initialTime: TimeOfDay.now(),
                                context: context,
                              );

                              if (pickedTime != null) {
                                print(pickedTime
                                    .format(context)); //output 10:51 PM
                                DateTime parsedTime = DateFormat.jm().parse(
                                    pickedTime.format(context).toString());
                                //converting to DateTime so that we can further format on different pattern.
                                print(
                                    parsedTime); //output 1970-01-01 22:53:00.000
                                String formattedTime =
                                    DateFormat('HH:mm:ss').format(parsedTime);
                                print(formattedTime); //output 14:59:00
                                //DateFormat() is from intl package, you can format the time on any pattern you need.

                                setState(() {
                                  timeinput.text =
                                      formattedTime; //set the value of text field.
                                });
                              } else {
                                Strings.ERROR_EMPTY;
                              }
                            },
                          ))),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: durationOfStaysDays,
                          decoration: InputDecoration(
                              labelText: User.LABEL_STAY,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: durationOfStaysHours,
                          decoration: InputDecoration(
                              labelText: User.LABEL_HOURS,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: adults,
                          decoration: InputDecoration(
                              labelText: User.LABEL_ADULTS,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: children,
                          decoration: InputDecoration(
                              labelText: User.LABEL_CHILDRENS,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: deposite,
                          decoration: InputDecoration(
                              labelText: User.LABEL_DEPOSIT,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: vehicleNo,
                          decoration: InputDecoration(
                              labelText: User.LABEL_VEHICAL_NO,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          controller: reference,
                          decoration: InputDecoration(
                              labelText: User.LABEL_REFERENCE,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: ElevatedButton(
                            onPressed: () {
                              //save
                              _doSaveCheckInDetails();
                            },
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Color(0xFF939ea5)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        side: BorderSide(color: Colors.grey)))),
                            child: Text(
                              Strings.TEXT_SAVE_DETAILS,
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: Strings.FONT_FAMILY_TEXT,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _doSaveCheckInDetails() {
    Util.log('_doSaveCheckInDetails', 'Save Details');
    Reservation reservation = Reservation(
        id: -1,
        customer: widget.customer,
        customerId: widget.customer.id,
        date: dateinput.text,
        time: timeinput.text,
        durationOfStaysDays: 3,
        durationOfStaysHours: 3,
        adults: 2,
        children: 1,
        deposit: 4000,
        vehicleNo: vehicleNo.text,
        reference: reference.text,
        status: Reservation.STATUS_NEW, //todo new or confirmed?
        remarks: "New Reservation!!"
    );
    String jsondata = jsonEncode(reservation);
    print(jsondata);
    //todo post data here to server!

    CheckInRequest checkinRequest =
    CheckInRequest(widget.customer, reservation);
    print(jsonEncode(checkinRequest));
  }
}
