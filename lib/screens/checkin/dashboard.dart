import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/screens/checkIn/details.dart';
import 'package:hotel_project/http/request/customer_request.dart';
import 'package:hotel_project/http/response/customer_response.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/models/photo.dart';
import 'package:hotel_project/screens/checkIn/new_customer.dart';
import 'package:hotel_project/common/strings.dart';

class CheckInScreen extends StatefulWidget {
  const CheckInScreen({Key? key}) : super(key: key);

  @override
  _CheckInScreenState createState() => _CheckInScreenState();
}

class _CheckInScreenState extends State<CheckInScreen> {
  String? customer;

  List<String> customerType = [
    'Existing Customer',
    'New Customer',
  ];

  late Future<CustomerListResponse> customerList;

  // late Future<User> user;
  Customer? selectedCustomer;
  late Future<Photo> futurePhoto;
  late String selectedCustomerType;

  @override
  void initState() {
    super.initState();
    Util.log("check_in_screen_dashboard", "init state");
    selectedCustomerType = customerType.elementAt(0);

    customerList = getCustomerList();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TITLE_CHECKIN_DASHBOARD),
        backgroundColor: Color(0xFF02bbd7),
      ),
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          Strings.TEXT_CUSTOMER_TYPE,
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: Strings.FONT_FAMILY_HEADING),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Row(
                      children: [
                        Expanded(
                          flex: 10,
                          child: Container(
                            height: 35,
                            width: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.grey)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                hint: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    selectedCustomerType,
                                    style: TextStyle(
                                        fontFamily: Strings.FONT_FAMILY_TEXT),
                                  ),
                                ),
                                items: customerType
                                    .map((item) => DropdownMenuItem<String>(
                                        value: item,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Text(
                                            item,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily:
                                                    Strings.FONT_FAMILY_TEXT),
                                          ),
                                        )))
                                    .toList(),
                                value: selectedCustomerType,
                                onChanged: (value) {
                                  setState(() {
                                    selectedCustomerType = value as String;
                                    print(Util.log("Customer Type",
                                        "${selectedCustomerType}"));
                                  });
                                },
                                buttonHeight: 10,
                                buttonWidth: 110,
                                itemHeight: 40,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    (selectedCustomerType == 'Existing Customer')
                        ? Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    Strings.TEXT_SELECT_CUSTOMER,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily:
                                            Strings.FONT_FAMILY_HEADING),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 10,
                                    child: Container(
                                      height: 35,
                                      width: 200,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border:
                                              Border.all(color: Colors.grey)),
                                      child: FutureBuilder<
                                              CustomerListResponse>(
                                          future: customerList,
                                          builder: (context, snapshot) {
                                            if (snapshot.hasData) {
                                              if (selectedCustomer == null)
                                                selectedCustomer = snapshot
                                                    .data?.customerList
                                                    .elementAt(0);

                                              return DropdownButtonHideUnderline(
                                                child: DropdownButton2(
                                                  items: snapshot
                                                      .data?.customerList
                                                      .map((item) =>
                                                          DropdownMenuItem<
                                                                  Customer>(
                                                              value: item,
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            10.0),
                                                                child: Text(
                                                                  item.name,
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      fontFamily:
                                                                          Strings
                                                                              .FONT_FAMILY_TEXT),
                                                                ),
                                                              )))
                                                      .toList(),
                                                  value: selectedCustomer,
                                                  onChanged: (username) {
                                                    setState(() {
                                                      selectedCustomer =
                                                          username as Customer;
                                                      Util.log(
                                                          'Selected Customer',
                                                          '${selectedCustomer}');
                                                    });
                                                  },
                                                  buttonHeight: 10,
                                                  buttonWidth: 110,
                                                  itemHeight: 40,
                                                ),
                                              );
                                            } else {
                                              return Text(Strings.TEXT_LOADING);
                                            }
                                          }),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          )
                        : SizedBox(
                            height: 0,
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: ElevatedButton(
                          onPressed: () {
                            (selectedCustomerType == "Existing Customer")
                                ? Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => CheckInDetails(
                                              customer: selectedCustomer!,
                                            )))
                                : Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            NewCustomerDetails()));
                            Util.log('Button', 'Next Clicked');
                          },
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Color(0xFF939ea5)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      side: BorderSide(color: Colors.grey)))),
                          child: Text(
                            Strings.TEXT_NEXT,
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
