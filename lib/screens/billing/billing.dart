import 'package:flutter/material.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:hotel_project/common/util.dart';

class Billing extends StatefulWidget {
  const Billing({Key? key}) : super(key: key);

  @override
  _BillingState createState() => _BillingState();
}

class _BillingState extends State<Billing> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TITLE_BILLING),
        backgroundColor: Color(0xFF02bbd7),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: IconButton(onPressed: (){
              // Navigator.push(context, MaterialPageRoute(builder: (context)=>RoomDetails()));
            }, icon: Icon(Icons.add)),
          )
        ],
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10,bottom: 10),
              child: Container(
                height:double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ID,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_PAYEE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Rashid Uddin",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOMS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "101",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_FOOD_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "140.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOM_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1500.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TAX_APPLIED,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "100.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DEPOSIT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "5000.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "3208",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.-1780.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_STATUS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Cash",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(onPressed:(){
                                  // _showDailoge();
                                  Util.log('Button', 'Print CLicked');
                                },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(18),
                                              side: BorderSide(color: Colors.grey)
                                          )
                                      )
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15.0,right: 15),
                                    child: Text(Strings.TEXT_PRINT,style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),),
                                  ),),
                              ),
                              SizedBox(height: 10,)
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ID,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_PAYEE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Rashid Uddin",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOMS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "101",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_FOOD_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "140.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOM_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1500.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TAX_APPLIED,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "100.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DEPOSIT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "5000.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "3208",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.-1780.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_STATUS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Cash",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(onPressed:(){
                                  // _showDailoge();
                                  Util.log('Button', 'Print CLicked');
                                },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(18),
                                              side: BorderSide(color: Colors.grey)
                                          )
                                      )
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15.0,right: 15),
                                    child: Text(Strings.TEXT_PRINT,style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),),
                                  ),),
                              ),
                              SizedBox(height: 10,)
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ID,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_PAYEE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Rashid Uddin",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOMS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "101",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_FOOD_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "140.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOM_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1500.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TAX_APPLIED,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "100.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DEPOSIT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "5000.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "3208",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.-1780.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_STATUS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Cash",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(onPressed:(){
                                  // _showDailoge();
                                  Util.log('Button', 'Print CLicked');
                                },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(18),
                                              side: BorderSide(color: Colors.grey)
                                          )
                                      )
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15.0,right: 15),
                                    child: Text(Strings.TEXT_PRINT,style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),),
                                  ),),
                              ),
                              SizedBox(height: 10,)
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ID,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_PAYEE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Rashid Uddin",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOMS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "101",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_FOOD_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "140.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOM_CHARGE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1500.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TAX_APPLIED,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "100.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DEPOSIT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "5000.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "3208",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.-1780.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_STATUS,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Cash",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5,),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: ElevatedButton(onPressed:(){
                                  // _showDailoge();
                                  Util.log('Button', 'Print CLicked');
                                },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(18),
                                              side: BorderSide(color: Colors.grey)
                                          )
                                      )
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 15.0,right: 15),
                                    child: Text(Strings.TEXT_PRINT,style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),),
                                  ),),
                              ),
                              SizedBox(height: 10,)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
