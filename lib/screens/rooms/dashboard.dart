import 'package:flutter/material.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/http/request/roomlist_request.dart';
import 'package:hotel_project/http/response/room_list_response.dart';
import 'package:hotel_project/models/room.dart';
import 'package:hotel_project/common/strings.dart';
import 'room_100.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'add_room.dart';

class RoomsDashboard extends StatefulWidget {
  const RoomsDashboard({Key? key}) : super(key: key);

  @override
  _RoomsDashboardState createState() => _RoomsDashboardState();
}

class _RoomsDashboardState extends State<RoomsDashboard> {
  late Future<RoomListResponse> futureRoomListResponse;
  String? selectedValue;
  String defaultValue = "All Rooms";
  List<String> items = ['All Rooms', 'Vacant', 'CheckIn', 'Reserved'];
  String radioButtonItem = "Reserved Rooms";
  int id = 1;
  List<Room> roomsList = [];
  List<Room> sourceRoomList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureRoomListResponse = getRoomList();

    // for (int i = 1; i < 19; i++) {
    //   roomsList.add(Room(i, "Room 10$i", (i % 3)));
    // }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Rooms",
          style: TextStyle(fontFamily: Strings.FONT_FAMILY_HEADING),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => RoomDetails()));
                },
                icon: Icon(Icons.add)),
          )
        ],
        backgroundColor: Color(0xFF02bbd7),
      ),
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Search Here...",
                        hintStyle:
                            TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                        suffixIcon: Icon(Icons.search),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8, top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Room Details",
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: Strings.FONT_FAMILY_HEADING),
                        ),
                        Container(
                          height: 30,
                          width: 150,
                          decoration: BoxDecoration(
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton2(
                              hint: Padding(
                                padding: const EdgeInsets.only(left: 2.0),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    defaultValue,
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: Strings.FONT_FAMILY_TEXT),
                                  ),
                                ),
                              ),
                              items: items
                                  .map((item) => DropdownMenuItem<String>(
                                        value: item,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 8.0),
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                                fontSize: 14,
                                                fontFamily:
                                                    Strings.FONT_FAMILY_TEXT),
                                          ),
                                        ),
                                      ))
                                  .toList(),
                              value: selectedValue,
                              onChanged: (value) {
                                print("onchange ${value}");
                                if (value == items[0]) {
                                  filterRooms(Room.STATUS_ALL);
                                } else if (value == items[1]) {
                                  filterRooms(Room.STATUS_VACANT);
                                } else if (value == items[2]) {
                                  filterRooms(Room.STATUS_CHEKED_IN);
                                } else if (value == items[3]) {
                                  filterRooms(Room.STATUS_RESERVED);
                                }

                                setState(() {
                                  selectedValue = value as String;
                                  print("dropdown value ${selectedValue} ");
                                });
                              },
                              buttonHeight: 10,
                              buttonWidth: 110,
                              itemHeight: 40,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              FutureBuilder<RoomListResponse>(
                  future: futureRoomListResponse,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      Util.log("room_dashboard_future_rooms", "data found!");
                      roomsList = snapshot.data!.roomList;
                      print("rooms list ${roomsList.length}");
                      return Text("Loaded");
                    } else {
                      return Text("Loading");
                    }
                  }),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                      child: GridView.builder(
                          scrollDirection: Axis.vertical,
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: size.width / 2,
                                  childAspectRatio: 3 / 2,
                                  crossAxisSpacing: 20,
                                  mainAxisSpacing: 20),
                          itemCount: roomsList.length,
                          itemBuilder: (BuildContext context, index) {
                            Room currentRoom = roomsList.elementAt(index);
                            return Card(
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Room100()));
                                },
                                child: Container(
                                  // height: 100,
                                  // width: 115,
                                  decoration: BoxDecoration(
                                    // border: Border.all(color: Color(0xFF577482)),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 0.0, top: 0),
                                          child:
                                              currentRoom.getRoomStatusIcon()),
                                      Divider(
                                        color: Color(0xFF577482),
                                      ),
                                      Text(
                                        currentRoom.roomCode,
                                        style: TextStyle(
                                            fontFamily:
                                                Strings.FONT_FAMILY_TEXT,
                                            fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          })),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void filterRooms(int val) {
    setState(() {
      print(" ${roomsList.length}");
      switch (val) {
        case -1:
          if (sourceRoomList.length > roomsList.length)
            roomsList = sourceRoomList;
          break;
        case Room.STATUS_RESERVED:
          if (sourceRoomList.length > roomsList.length)
            roomsList = sourceRoomList;
          sourceRoomList = roomsList;

          roomsList = [];
          for (int i = 0; i < sourceRoomList.length; i++) {
            Room iterationRoom = sourceRoomList[i];
            if (iterationRoom.status == Room.STATUS_RESERVED)
              roomsList.add(iterationRoom);
          }
          break;
        case Room.STATUS_VACANT:
          if (sourceRoomList.length > roomsList.length)
            roomsList = sourceRoomList;
          sourceRoomList = roomsList;

          roomsList = [];
          for (int i = 0; i < sourceRoomList.length; i++) {
            Room iterationRoom = sourceRoomList[i];
            if (iterationRoom.status == Room.STATUS_VACANT)
              roomsList.add(iterationRoom);
          }
          break;
        case Room.STATUS_CHEKED_IN:
          if (sourceRoomList.length > roomsList.length)
            roomsList = sourceRoomList;
          sourceRoomList = roomsList;

          roomsList = [];
          for (int i = 0; i < sourceRoomList.length; i++) {
            Room iterationRoom = sourceRoomList[i];
            if (iterationRoom.status == Room.STATUS_CHEKED_IN)
              roomsList.add(iterationRoom);
          }
          break;
      }
      print("setting value $val");
      id = val as int;
    });
  }
}
