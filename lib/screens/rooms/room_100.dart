import 'package:flutter/material.dart';
import 'package:hotel_project/common/strings.dart';
import 'room100_info.dart';
import 'room100_schedule.dart';
import 'room100_customer.dart';
import 'room100_add.dart';

class Room100 extends StatefulWidget {
  const Room100({Key? key}) : super(key: key);

  @override
  _Room100State createState() => _Room100State();
}

class _Room100State extends State<Room100> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Room",style: TextStyle(
                fontFamily: Strings.FONT_FAMILY_HEADING,
            ),),
            backgroundColor: Color(0xFF02bbd7),
            elevation: 0,
            bottom: TabBar(
              unselectedLabelColor:Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF939ea5),
                      Color(0xFF939ea5)
                    ]),
                borderRadius: BorderRadius.circular(5),),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("INFO"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Schedule"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Customer",style: TextStyle(
                      fontSize: 13
                    ),),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Icon(Icons.add),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Room100TabBar(),
              Calendar(),
              Room100Customer(),
              Room100Add(),
            ],
          ),
        )
    );
  }
}
