import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/strings.dart';

class Room100Add extends StatefulWidget {
  const Room100Add({Key? key}) : super(key: key);

  @override
  _Room100AddState createState() => _Room100AddState();
}

class _Room100AddState extends State<Room100Add> {
  final _formkey = GlobalKey<FormState>();
  String? selectedValue;
  String defalutValue = "22/02/22";
  List<String> items = [
    '22/02/2022',
    '23/02/2022',
    '24/02/2022',
    '25/02/2022',
  ];
  String? selectedAdults;
  String defalutAdults = "1";
  List<String> itemAdults = [
    '1',
    '2',
    '3',
    '4',
  ];
  String? selectedFemale;
  String defalutFemales = "2";
  List<String> itemFemale = [
  '1',
  '2',
  '3',
  '4',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0,right: 10,top: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text("Date",style:TextStyle(
                              fontSize: 14,
                              fontFamily: Strings.FONT_FAMILY_HEADING),)
                        ],
                      ),
                      SizedBox(height: 3,),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutValue,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: items
                                      .map((item) => DropdownMenuItem<String>(
                                      value: item,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          item,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: Strings.FONT_FAMILY_TEXT),
                                        ),
                                      )))
                                      .toList(),
                                  value: selectedValue,
                                  onChanged: (value) {
                                    setState(() {
                                      selectedValue = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Name",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "CNIC",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(height: 14,),
                      Row(
                        children: [
                          Text("Adults",style:TextStyle(
                              fontSize: 14,
                              fontFamily: Strings.FONT_FAMILY_HEADING),)
                        ],
                      ),
                      SizedBox(height: 3,),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutAdults,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: itemAdults
                                      .map((item) => DropdownMenuItem<String>(
                                      value: item,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          item,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: Strings.FONT_FAMILY_TEXT),
                                        ),
                                      )))
                                      .toList(),
                                  value: selectedAdults,
                                  onChanged: (value) {
                                    setState(() {
                                      selectedAdults = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 14,),
                      Row(
                        children: [
                          Text("Females",style:TextStyle(
                              fontSize: 14,
                              fontFamily: Strings.FONT_FAMILY_HEADING),)
                        ],
                      ),
                      SizedBox(height: 3,),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutFemales,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: itemFemale
                                      .map((item) => DropdownMenuItem<String>(
                                      value: item,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(left: 10.0),
                                        child: Text(
                                          item,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: Strings.FONT_FAMILY_TEXT),
                                        ),
                                      )))
                                      .toList(),
                                  value: selectedFemale,
                                  onChanged: (value) {
                                    setState(() {
                                      selectedFemale = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 10,),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: ElevatedButton(
                            onPressed:(){
                              if (_formkey.currentState!.validate()) {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => CheckInDetails()),
                                // );
                              }
                            // _showDailoge();
                            print("reserved button clicked");
                          },
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        side: BorderSide(color: Colors.grey)
                                    )
                                )
                            ),
                            child: Text("Reserved",style: TextStyle(
                                fontSize: 15,
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                            ),),),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
