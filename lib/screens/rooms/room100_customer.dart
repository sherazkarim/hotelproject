import 'package:flutter/material.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/screens/rooms/room_customer_details.dart';
import 'package:hotel_project/common/strings.dart';
import 'customer/tabbar.dart';

class Room100Customer extends StatefulWidget {
  const Room100Customer({Key? key}) : super(key: key);

  @override
  _Room100CustomerState createState() => _Room100CustomerState();
}

class _Room100CustomerState extends State<Room100Customer> {
  List<User> userList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userList = [
      User(id: 1,name: 'Azmat karim',email: "email@user.com", username:"username@"),
      User(id: 2,name: 'Aqeel',email: "email@user.com", username:"username@"),
      User(id: 3,name: 'Tariq Mehmood',email: "email@user.com", username:"username@"),
      User(id: 4,name: 'Zahid',email: "email@user.com", username:"username@"),
      User(id: 5,name: 'Sheraz',email: "email@user.com", username:"username@"),
    ];
  }
  Future<void> _showDailoge() async{
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
          contentPadding: EdgeInsets.all(10),
          title: Center(
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Text(
                      'Are you sure???',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black),
                    ),
                    SizedBox(height: 30,),
                  ],
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(           // FlatButton widget is used to make a text to work like a button
              textColor: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },        // function used to perform after pressing the button
              child: Text('Yes'),
            ),
            FlatButton(
              textColor: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('No'),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: Container(
              child: ListView.builder(
                  itemCount: userList.length,
                  itemBuilder: (BuildContext context, int index) {
                    User currentdata = userList[index];
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>CustomerTabBar()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0, right: 10),
                              child: Card(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10, right: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        flex: 7,
                                        child: Align(
                                          // alignment: Alignment.centerLeft,
                                          child: Column(
                                            children: [
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  currentdata.name,
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'Rubik,',
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  currentdata.note,
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: Strings.FONT_FAMILY_TEXT,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Row(
                                          children: [
                                            // Container(
                                            //   height: 50,
                                            //   width: 1,
                                            //   color: Colors.grey,
                                            // ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Column(
                                              children: [
                                                ElevatedButton(onPressed:(){
                                                  _showDailoge();
                                                },
                                                  style: ButtonStyle(
                                                      backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                      RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(18),
                                                        side: BorderSide(color: Colors.grey)
                                                      )
                                                    )
                                                  ),
                                                child: Text("Cancel",style: TextStyle(
                                                  fontSize: 13
                                                ),),),
                                                Text(
                                                  currentdata.status,
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    fontFamily: Strings.FONT_FAMILY_TEXT,
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
        ),

      )
    );
  }
}
