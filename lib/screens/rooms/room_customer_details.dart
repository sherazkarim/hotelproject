import 'package:flutter/material.dart';
import 'package:hotel_project/common/strings.dart';

class CustomerDetails extends StatefulWidget {
  const CustomerDetails({Key? key}) : super(key: key);

  @override
  _CustomerDetailsState createState() => _CustomerDetailsState();
}

class _CustomerDetailsState extends State<CustomerDetails> {
  final _formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Form(
                      key: _formkey,
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "ID",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Guest",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Room",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "CheckIn Date",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "CheckIn Time",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Vehicle No",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Deposite",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Status",
                                  labelStyle: TextStyle(
                                      fontFamily: Strings.FONT_FAMILY_TEXT
                                  )
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "please fill this field";
                                }
                                return null;
                              },
                            ),
                          ),
                          SizedBox(height: 10,),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: ElevatedButton(
                              onPressed: () {
                                if (_formkey.currentState!.validate()) {
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(
                                  //       builder: (context) => CheckInDetails()),
                                  // );
                                  print("check in button clicked");
                                }
                              },
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(18),
                                          side: BorderSide(color: Colors.grey)
                                      )
                                  )
                              ),
                              child: Text("CheckOut",style: TextStyle(
                                  fontSize: 13
                              ),),),
                          ),
                          SizedBox(height: 10,),
                        ],
                      ),
                    ),
                  )
                ),
              ),
          ),

        ),
      ),
    );
  }
}
