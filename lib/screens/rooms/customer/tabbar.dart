import 'package:flutter/material.dart';
import 'package:hotel_project/screens/rooms/customer/add_services.dart';
import 'package:hotel_project/screens/rooms/room_customer_details.dart';
import 'package:hotel_project/screens/rooms/customer/services_table.dart';
import 'package:hotel_project/common/strings.dart';

class CustomerTabBar extends StatefulWidget {
  const CustomerTabBar({Key? key}) : super(key: key);

  @override
  _CustomerTabBarState createState() => _CustomerTabBarState();
}

class _CustomerTabBarState extends State<CustomerTabBar> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Room",style: TextStyle(
              fontFamily: Strings.FONT_FAMILY_HEADING,
            ),),
            backgroundColor: Color(0xFF02bbd7),
            elevation: 0,
            bottom: TabBar(
              unselectedLabelColor:Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF939ea5),
                      Color(0xFF939ea5)
                    ]),
                borderRadius: BorderRadius.circular(5),),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Room"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Services"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Icon(Icons.add),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              CustomerDetails(),
              CustomerServicesTable(),
              CustomerAddServices(),

            ],
          ),
        )
    );
  }
}

