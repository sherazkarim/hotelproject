import 'package:flutter/material.dart';
import 'package:hotel_project/common/strings.dart';

class CustomerServicesTable extends StatefulWidget {
  const CustomerServicesTable({Key? key}) : super(key: key);

  @override
  _CustomerServicesTableState createState() => _CustomerServicesTableState();
}

class _CustomerServicesTableState extends State<CustomerServicesTable> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
              padding: EdgeInsets.all(5),
              child:Table(
                border: TableBorder.all(width:1, color:Colors.black45), //table border
                children: const [

                  TableRow(
                      children: [
                        TableCell(child: Text("S/N",style: TextStyle(
                          fontFamily: Strings.FONT_FAMILY_HEADING,
                          fontSize: 13
                        ),)),
                        TableCell(child: Text("Items",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_HEADING,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Price",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_HEADING,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Quantity",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_HEADING,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Discount",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_HEADING,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Sub Total",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_HEADING,
                            fontSize: 13
                        ))),
                      ]
                  ),

                  TableRow(
                      children: [
                        TableCell(child: Text("1.",style: TextStyle(
                      fontFamily: Strings.FONT_FAMILY_TEXT,
                      fontSize: 13
                  ))),
                        TableCell(child: Text("Chicken Tikka",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.300",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("1",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("10%",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.300",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        )))
                      ]
                  ),
                  TableRow(
                      children: [
                        TableCell(child: Text("1.",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Chicken Tikka",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.300",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("1",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("10%",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.300",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        )))
                      ]
                  ),
                  TableRow(
                      children: [
                        TableCell(child: Text("1.",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Chicken Tikka",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.300",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("1",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("10%",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.300",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        )))
                      ]
                  ),
                  TableRow(
                      children: [
                        TableCell(child: Text(" ",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text(" ",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text(" ",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text(" ",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Total",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_HEADING,
                            fontSize: 13
                        ))),
                        TableCell(child: Text("Rs.2000",style: TextStyle(
                            fontFamily: Strings.FONT_FAMILY_TEXT,
                            fontSize: 13
                        )))
                      ]
                  ),

                ],)
          )
    );
  }
}
