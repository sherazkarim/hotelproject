import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/strings.dart';

class CustomerAddServices extends StatefulWidget {
  const CustomerAddServices({Key? key}) : super(key: key);

  @override
  _CustomerAddServicesState createState() => _CustomerAddServicesState();
}

class _CustomerAddServicesState extends State<CustomerAddServices> {
  final _formkey = GlobalKey<FormState>();
  String? selectedFood;
  String defalutFood = "Food";
  List<String> foodAvailable = [
    'Nihari',
    'Qourma',
    'Chicken Handi',
    'Mix Sabzi',
    'Soup',
  ];
  String? discount;
  String defalutDiscount = "N/A";
  List<String> discountdetails = [
    '5%',
    '10%',
    '15%',
    '20%',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text("Choose Food",style:TextStyle(
                      fontSize: 14,
                      fontFamily: Strings.FONT_FAMILY_HEADING),)
                        ],
                      ),
                      SizedBox(height: 3,),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutFood,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: foodAvailable
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              item,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: Strings.FONT_FAMILY_TEXT),
                                            ),
                                          )))
                                      .toList(),
                                  value: selectedFood,
                                  onChanged: (value) {
                                    setState(() {
                                      selectedFood = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      // SizedBox(height: 5,),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Unit Price",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      // SizedBox(height: 5,),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Quantity",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(height: 14,),
                      Row(
                        children: [
                          Text("Discount Type",style:TextStyle(
                              fontSize: 14,
                              fontFamily: Strings.FONT_FAMILY_HEADING),)
                        ],
                      ),
                      SizedBox(height: 3,),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutDiscount,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: discountdetails
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              item,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: Strings.FONT_FAMILY_TEXT),
                                            ),
                                          )))
                                      .toList(),
                                  value: discount,
                                  onChanged: (value) {
                                    setState(() {
                                      discount = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      // SizedBox(height: 5,),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Discount",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formkey.currentState!.validate()) {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => CheckInDetails()),
                                // );
                              }
                              // _showDailoge();
                              print("reserved button clicked");
                            },
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Color(0xFF939ea5)),
                                shape:
                                    MaterialStateProperty.all<RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(18),
                                            side: BorderSide(color: Colors.grey)))),
                            child: Text(
                              "Place Order",
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: Strings.FONT_FAMILY_TEXT,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
