import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/strings.dart';

class Room100TabBar extends StatefulWidget {
  const Room100TabBar({Key? key}) : super(key: key);

  @override
  _Room100TabBarState createState() => _Room100TabBarState();
}

class _Room100TabBarState extends State<Room100TabBar> {
  final _formkey = GlobalKey<FormState>();
  late List<CompanyWidget> _companies;
  late List<String> _filters;

  @override
  void initState() {
    super.initState();
    _filters = <String>[];
    _companies = <CompanyWidget>[
      CompanyWidget('Hot Water'),
      CompanyWidget('Exaust'),
      CompanyWidget('Soap'),
      CompanyWidget('Brush'),
    ];
  }

  String? selectedFloor;
  String defaultFloor = "1st Floor";
  List<String> floors = [
    'Ground Floor',
    '1st Floor',
    '2nd Floor',
    'Third Floor'
  ];
  String? selectRoomType;
  String defalutRoomType = "Single Beded";
  List<String> roomType = [
    'Single Beded',
    'Double Beded',
    '3 Beded',
    '4 Beded',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Code",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Row(
                        children: [
                          Text(
                            "Choose Floor",
                            style: TextStyle(fontSize: 14, fontFamily: Strings.FONT_FAMILY_HEADING),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defaultFloor,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: floors
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              item,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: Strings.FONT_FAMILY_TEXT),
                                            ),
                                          )))
                                      .toList(),
                                  value: selectedFloor,
                                  onChanged: (value) {
                                    setState(() {
                                      selectedFloor = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Row(
                        children: [
                          Text(
                            "Select Room Type",
                            style: TextStyle(fontSize: 14, fontFamily: Strings.FONT_FAMILY_HEADING),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutRoomType,
                                      style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: roomType
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              item,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: Strings.FONT_FAMILY_TEXT),
                                            ),
                                          )))
                                      .toList(),
                                  value: selectRoomType,
                                  onChanged: (value) {
                                    setState(() {
                                      selectRoomType = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Capacity",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Rent",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: "Description",
                              labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "please fill this field";
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text(
                            "Select Services",
                            style: TextStyle(fontSize: 14, fontFamily: Strings.FONT_FAMILY_HEADING),
                          )
                        ],
                      ),
                      Wrap(
                        children: companyPosition.toList(),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                          Padding(
                            padding: const EdgeInsets.only(right: 0.0),
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: ElevatedButton(
                                  onPressed: () {
                                    if (_formkey.currentState!.validate()) {
                                      // Navigator.push(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //       builder: (context) => CheckInDetails()),
                                      // );
                                      print("update button clicked");
                                    }

                                },
                                style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        Color(0xFF939ea5)),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            side:
                                                BorderSide(color: Colors.grey)))),
                                child: Text(
                                  "Update Room",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: Strings.FONT_FAMILY_TEXT,
                                  ),
                                ),
                              ),
                            ),
                          ),

                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Iterable<Widget> get companyPosition sync* {
    for (CompanyWidget company in _companies) {
      yield Padding(
        padding: const EdgeInsets.all(6.0),
        child: FilterChip(
          backgroundColor: Colors.tealAccent[200],
          avatar: CircleAvatar(
            backgroundColor: Colors.cyan,
            child: Text(
              company.name[0].toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
          ),
          label: Text(
            company.name,
          ),
          selected: _filters.contains(company.name),
          selectedColor: Color(0xFF02bbd7),
          onSelected: (bool selected) {
            setState(() {
              if (selected) {
                _filters.add(company.name);
              } else {
                _filters.removeWhere((String name) {
                  return name == company.name;
                });
              }
            });
          },
        ),
      );
    }
  }
}

class CompanyWidget {
  const CompanyWidget(this.name);

  final String name;
}
