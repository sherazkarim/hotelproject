import 'package:flutter/material.dart';
import 'package:hotel_project/screens/settings/services.dart';
import 'package:hotel_project/common/strings.dart';

class SettingsDashboard extends StatefulWidget {
  const SettingsDashboard({Key? key}) : super(key: key);

  @override
  _SettingsDashboardState createState() => _SettingsDashboardState();
}

class _SettingsDashboardState extends State<SettingsDashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TEXT_SETTING),
        backgroundColor: Color(0xFF44b2c9),
      ),
      body:Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>Services()));
                    },
                    child: Container(
                      height: 105,
                      width: 115,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Color(0xFF577482)),
                        borderRadius: BorderRadius.circular(5),
                        // color: Colors.white
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0,top: 6),
                            child: Icon(Icons.home_repair_service_outlined,size: 50,color: Color(0xFF02bbd7),),
                          ),
                          Divider(color: Color(0xFF577482)),
                          Text(Strings.TEXT_SERVICES,style: TextStyle(
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                              fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  child: GestureDetector(
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>RoomsDashboard()));
                    },
                    child: Container(
                      height: 105,
                      width: 115,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Color(0xFF577482)),
                        borderRadius: BorderRadius.circular(5),
                        // color: Colors.white
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0,top: 6),
                            child: Icon(Icons.pending_outlined,size: 50,color: Color(0xFF02bbd7),),
                          ),
                          Divider(color: Color(0xFF577482)),
                          Text("Comming Soon",style: TextStyle(
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                              fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  child: GestureDetector(
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>ReservationsTabBar()));
                    },
                    child: Container(
                      height: 105,
                      width: 115,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Color(0xFF577482)),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0,top: 6),
                            child: Icon(Icons.pending_outlined,size: 50,color: Color(0xFF02bbd7),),
                          ),
                          Divider(color: Color(0xFF577482)),
                          Text("Comming Soon",style: TextStyle(
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                              fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  child: GestureDetector(
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>Reports()));
                    },
                    child: Container(
                      height: 105,
                      width: 115,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Color(0xFF577482)),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0,top: 6),
                            child: Icon(Icons.pending_outlined,size: 50,color: Color(0xFF02bbd7),),
                          ),
                          Divider(color: Color(0xFF577482)),
                          Text("Comming Soon",style: TextStyle(
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                              fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 40,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  child: GestureDetector(
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                    },
                    child: Container(
                      height: 105,
                      width: 115,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Color(0xFF577482)),
                        borderRadius: BorderRadius.circular(5),
                        // color: Colors.white
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0,top: 6),
                            child: Icon(Icons.pending_outlined,size: 50,color: Color(0xFF02bbd7),),
                          ),
                          Divider(color: Color(0xFF577482)),
                          Text("Comming Soon",style: TextStyle(
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                              fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  child: GestureDetector(
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                    },
                    child: Container(
                      height: 105,
                      width: 115,
                      decoration: BoxDecoration(
                        // border: Border.all(color: Color(0xFF577482)),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0,top: 6),
                            child: Icon(Icons.logout,size: 50,color: Color(0xFF02bbd7),),
                          ),
                          Divider(color: Color(0xFF577482)),
                          Text(Strings.TEXT_LOGOUT,style: TextStyle(
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                              fontWeight: FontWeight.bold
                          ),)
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

