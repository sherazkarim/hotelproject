import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:hotel_project/models/user.dart';
class Services extends StatefulWidget {
  const Services({Key? key}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  String? selectedCategory;
  String defalutCategory = "A";
  List<String> categories = [
    'A',
    'B',
    'C',
    'D',
  ];
  String? taxType;
  String defalutTax = "N/A";
  List<String> taxdetails = [
    '5%',
    '10%',
    '15%',
    '20%',
  ];
  String? isReady;
  String defaultReady = "No";
  List<String> ready = [
    'Yes',
    'No',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TITLE_SERVICES),
        backgroundColor: Color(0xFF44b2c9),
      ),
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10,),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: User.LABEL_CODE,
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Item Name",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Cost",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Price",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Text("Category",style:TextStyle(
                            fontSize: 14,
                            fontFamily: Strings.FONT_FAMILY_HEADING),)
                      ],
                    ),
                    SizedBox(height: 3,),
                    Row(
                      children: [
                        Expanded(
                          flex: 10,
                          child: Container(
                            height: 35,
                            width: 200,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                hint: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    defalutCategory,
                                    style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                  ),
                                ),
                                items: categories
                                    .map((item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(left: 10.0),
                                      child: Text(
                                        item,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: Strings.FONT_FAMILY_TEXT),
                                      ),
                                    )))
                                    .toList(),
                                value: selectedCategory,
                                onChanged: (value) {
                                  setState(() {
                                    selectedCategory = value as String;
                                  });
                                },
                                buttonHeight: 10,
                                buttonWidth: 110,
                                itemHeight: 40,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Unit",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Text("Tax Type",style:TextStyle(
                            fontSize: 14,
                            fontFamily: Strings.FONT_FAMILY_HEADING),)
                      ],
                    ),
                    SizedBox(height: 3,),
                    Row(
                      children: [
                        Expanded(
                          flex: 10,
                          child: Container(
                            height: 35,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                hint: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    defalutTax,
                                    style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                  ),
                                ),
                                items: taxdetails
                                    .map((item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(left: 10.0),
                                      child: Text(
                                        item,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: Strings.FONT_FAMILY_TEXT),
                                      ),
                                    )))
                                    .toList(),
                                value: taxType,
                                onChanged: (value) {
                                  setState(() {
                                    taxType= value as String;
                                  });
                                },
                                buttonHeight: 10,
                                buttonWidth: 110,
                                itemHeight: 40,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    // SizedBox(height: 5,),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Tax",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Quantity Available",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      child: TextField(
                        decoration: InputDecoration(
                            labelText: "Preparation Duration",
                            labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                        obscureText: true,
                      ),
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Text("It is ready?",style:TextStyle(
                            fontSize: 14,
                            fontFamily: Strings.FONT_FAMILY_HEADING),)
                      ],
                    ),
                    SizedBox(height: 3,),
                    Row(
                      children: [
                        Expanded(
                          flex: 10,
                          child: Container(
                            height: 35,
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey)),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                hint: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    defaultReady,
                                    style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                  ),
                                ),
                                items: ready
                                    .map((item) => DropdownMenuItem<String>(
                                    value: item,
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(left: 10.0),
                                      child: Text(
                                        item,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily: Strings.FONT_FAMILY_TEXT),
                                      ),
                                    )))
                                    .toList(),
                                value: isReady,
                                onChanged: (value) {
                                  setState(() {
                                    isReady= value as String;
                                  });
                                },
                                buttonHeight: 10,
                                buttonWidth: 110,
                                itemHeight: 40,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            // _showDailoge();
                            print("services add item button clicked");
                          },
                          style: ButtonStyle(
                              backgroundColor:
                              MaterialStateProperty.all(Color(0xFF939ea5)),
                              shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      side: BorderSide(color: Colors.grey)))),
                          child: Text(
                            "Add Item",
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: Strings.FONT_FAMILY_TEXT,
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            // _showDailoge();
                            print("services cancel button clicked");
                          },
                          style: ButtonStyle(
                              backgroundColor:
                              MaterialStateProperty.all(Color(0xFF939ea5)),
                              shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18),
                                      side: BorderSide(color: Colors.grey)))),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0,right: 8),
                            child: Text(
                              "Cancel",
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: Strings.FONT_FAMILY_TEXT,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10,)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
