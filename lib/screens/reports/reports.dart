import 'package:flutter/material.dart';
import 'package:hotel_project/models/report.dart';
import 'package:hotel_project/models/room.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_charts/sparkcharts.dart';
import 'package:hotel_project/screens/dashboard.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class Reports extends StatefulWidget {
  const Reports({Key? key}) : super(key: key);

  @override
  _ReportsState createState() => _ReportsState();
}

class _ReportsState extends State<Reports> {

  // Future<void> _dailyReport() async{
  //   Column(
  //       children: [
  //         //Initialize the chart widget
  //         SfCartesianChart(
  //             primaryXAxis: CategoryAxis(),
  //             // Chart title
  //             title: ChartTitle(text: 'Half yearly Rooms analysis'),
  //             // Enable legend
  //             legend: Legend(isVisible: true),
  //             // Enable tooltip
  //             tooltipBehavior: TooltipBehavior(enable: true),
  //             series: <ChartSeries<_SalesData, String>>[
  //               LineSeries<_SalesData, String>(
  //                   dataSource: data,
  //                   xValueMapper: (_SalesData sales, _) => sales.year,
  //                   yValueMapper: (_SalesData sales, _) => sales.sales,
  //                   // name: 'Sales',
  //                   // Enable data label
  //                   dataLabelSettings: DataLabelSettings(isVisible: true))
  //             ]),
  //         Text("Sales"),
  //       ]);
  // }

  List<Report> reportList = [];
  List<Report> sourceList = [];
  int id =1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    reportList.add(Report(status: -1));
  }

  List<_SalesData> data = [
    _SalesData('Jan', 35),
    _SalesData('Feb', 28),
    _SalesData('Mar', 34),
    _SalesData('Apr', 32),
    _SalesData('May', 40)
  ];
  List<_SalesData> dataYearly = [
    _SalesData('Jan', 20),
    _SalesData('Feb', 40),
    _SalesData('Mar', 45),
    _SalesData('Apr', 18),
    _SalesData('May', 50)
  ];
  String? selectedValue;
  List<String> items = ['Daily', 'Weekly', 'Monthly'];
  String defaultValue = "Daily";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Reports'),
          backgroundColor: Color(0xFF02bbd7),

        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8, top: 10, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Room Details",
                            style: TextStyle(fontSize: 15, fontFamily: Strings.FONT_FAMILY_HEADING),
                          ),
                          Container(
                            height: 30,
                            width: 150,
                            decoration: BoxDecoration(
                              border: Border.all(),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton2(
                                hint: Padding(
                                  padding: const EdgeInsets.only(left: 2.0),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      defaultValue,
                                      style: TextStyle(
                                          fontSize: 13, fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                ),
                                items: items
                                    .map((item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Padding(
                                    padding:
                                    const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      item,
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                ))
                                    .toList(),
                                value: selectedValue,
                                onChanged: (value) {
                                  if (value == items[0]) {
                                    filterReports(Report.REPORT_DAILY);
                                  } else if (value == items[1]) {
                                    filterReports(Report.REPORT_WEEKLY);
                                  } else if (value == items[2]) {
                                    filterReports(Report.REPORT_MONTHLY);
                                  }

                                  setState(() {
                                    selectedValue = value as String;
                                    // print("dropdown value ${selectedValue} ");
                                  });
                                },
                                buttonHeight: 10,
                                buttonWidth: 110,
                                itemHeight: 40,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            )
            // child: Column(
            //     children: [
            //   //Initialize the chart widget
            //   SfCartesianChart(
            //       primaryXAxis: CategoryAxis(),
            //       // Chart title
            //       title: ChartTitle(text: 'Half yearly Rooms analysis'),
            //       // Enable legend
            //       legend: Legend(isVisible: true),
            //       // Enable tooltip
            //       tooltipBehavior: TooltipBehavior(enable: true),
            //       series: <ChartSeries<_SalesData, String>>[
            //         LineSeries<_SalesData, String>(
            //             dataSource: data,
            //             xValueMapper: (_SalesData sales, _) => sales.year,
            //             yValueMapper: (_SalesData sales, _) => sales.sales,
            //             // name: 'Sales',
            //             // Enable data label
            //             dataLabelSettings: DataLabelSettings(isVisible: true))
            //       ]),
            //       Text("Sales"),
            // ]),
          ),
        ));
  }
  void filterReports(int val) {
    setState(() {
      switch (val) {
        case -1:
          if (sourceList.length > reportList.length)
            reportList = sourceList;
          break;
        case Report.REPORT_DAILY:
          if (sourceList.length > reportList.length)
            reportList = sourceList;
          sourceList = reportList;

          reportList = [];
          for (int i = 0; i < sourceList.length; i++) {
            Report iterationReport = sourceList[i];
            if (iterationReport.status == Report.REPORT_DAILY)
              reportList.add(iterationReport);
          }
          break;
        case Report.REPORT_WEEKLY:
          if (sourceList.length > reportList.length)
            reportList = sourceList;
          sourceList = reportList;

          reportList = [];
          for (int i = 0; i < sourceList.length; i++) {
            Report iterationReport = sourceList[i];
            if (iterationReport.status == Report.REPORT_WEEKLY)
              reportList.add(iterationReport);
          }
          break;
        case Report.REPORT_MONTHLY:
          if (sourceList.length > reportList.length)
            reportList = sourceList;
          sourceList = reportList;

          reportList = [];
          for (int i = 0; i < sourceList.length; i++) {
            Report iterationReport = sourceList[i];
            if (iterationReport.status == Report.REPORT_MONTHLY)
              reportList.add(iterationReport);
          }
          break;
      }
      print("setting value $val");
      id = val as int;
    });
  }
}

class _SalesData {
  _SalesData(this.year, this.sales);

  final String year;
  final double sales;
}

