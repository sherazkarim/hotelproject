import 'package:flutter/material.dart';
import 'package:hotel_project/screens/reports/overview.dart';

class OverViewTabBar extends StatefulWidget {
  const OverViewTabBar({Key? key}) : super(key: key);

  @override
  _OverViewTabBarState createState() => _OverViewTabBarState();
}

class _OverViewTabBarState extends State<OverViewTabBar> with SingleTickerProviderStateMixin{
  TabController? _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TabBar(
              unselectedLabelColor: Colors.black,
              labelColor: Color(0xFF02bbd7),
              tabs: [
                Tab(
                  text: 'Daily',
                ),
                Tab(
                  text: 'Weekly',
                ),
                Tab(
                  text: 'Monthly',
                )
              ],
              controller: _tabController,
              indicatorSize: TabBarIndicatorSize.tab,
            ),
            Expanded(
              child: TabBarView(
                children: [
                  OverView(),
                  OverView(),
                  OverView(),
                ],
                controller: _tabController,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
