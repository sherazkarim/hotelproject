import 'package:flutter/material.dart';
import 'package:hotel_project/screens/reports/check.dart';
import 'package:hotel_project/screens/reports/overview.dart';
import 'package:hotel_project/common/strings.dart';

class ReportTabBar extends StatefulWidget {
  const ReportTabBar({Key? key}) : super(key: key);

  @override
  _ReportTabBarState createState() => _ReportTabBarState();
}

class _ReportTabBarState extends State<ReportTabBar> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Reports",style: TextStyle(
                fontFamily: Strings.FONT_FAMILY_HEADING
            ),),
            backgroundColor: Color(0xFF02bbd7),
            elevation: 0,
            bottom: TabBar(
              unselectedLabelColor:Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF939ea5),
                      Color(0xFF939ea5)
                    ]),
                borderRadius: BorderRadius.circular(5),),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Overview",style: TextStyle(
                        fontFamily: Strings.FONT_FAMILY_HEADING
                    ),),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Rooms",style: TextStyle(
                        fontFamily: Strings.FONT_FAMILY_HEADING
                    ),),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              OverViewTabBar(),
              OverView(),
            ],
          ),
        )
    );
  }
}
