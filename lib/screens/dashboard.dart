import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_project/screens/billing/billing.dart';
import 'package:hotel_project/screens/checkin/dashboard.dart';
import 'package:hotel_project/screens/checkout/billing_screen.dart';
import 'package:hotel_project/screens/service_order/dashboard.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:hotel_project/screens/rooms/dashboard.dart';
import 'package:hotel_project/screens/settings/dashboard.dart';
import 'reservations/dashboard.dart';
import 'reports/reports.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.TITLE_DASHBOARD),
        backgroundColor: Strings.PRIMARY_COLOR,
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CheckInScreen()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                          // color: Colors.white
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.check_box,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_CHECK_IN,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BillingScreen()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                          // color: Colors.white
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.check_box_outline_blank,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_CHECK_OUT,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RoomsDashboard()));
                        // Navigator.push(context, MaterialPageRoute(builder: (context)=>ReservationsTabBar()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.meeting_room_sharp,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_ROOMS,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ReservationScreen()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.home_repair_service_outlined,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_RESERVATIONS,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Reports()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                          // color: Colors.white
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.notes_sharp,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_REPORTS,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ServiceOrderScreen()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.reorder,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_SERVICE_ORDER,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Billing()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.note_sharp,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_BILLING,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SettingsDashboard()));
                      },
                      child: Container(
                        height: 105,
                        width: 115,
                        decoration: BoxDecoration(
                          // border: Border.all(color: Color(0xFF577482)),
                          borderRadius: BorderRadius.circular(5),
                          // color: Colors.white
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 4.0, top: 6),
                              child: Icon(
                                Icons.settings,
                                size: 50,
                                color: Strings.PRIMARY_COLOR,
                              ),
                            ),
                            Divider(color: Strings.SECONDARY_COLOR),
                            Text(
                              Strings.TEXT_SETTING,
                              style: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
