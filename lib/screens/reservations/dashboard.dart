import 'package:flutter/material.dart';
import 'package:hotel_project/screens/reservations/pending.dart';
import 'package:hotel_project/screens/reservations/home_page.dart';
import 'package:hotel_project/common/strings.dart';

class ReservationScreen extends StatefulWidget {
  const ReservationScreen({Key? key}) : super(key: key);

  @override
  _ReservationScreenState createState() => _ReservationScreenState();
}

class _ReservationScreenState extends State<ReservationScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text(Strings.TEXT_RESERVATIONS,style: TextStyle(
                fontFamily: Strings.FONT_FAMILY_HEADING
            ),),
            backgroundColor: Color(0xFF02bbd7),
            elevation: 0,
            bottom: TabBar(
              unselectedLabelColor:Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [
                        Color(0xFF939ea5),
                        Color(0xFF939ea5)
                      ]),
                  borderRadius: BorderRadius.circular(5),),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_ALL,style: TextStyle(
                        fontFamily: Strings.FONT_FAMILY_HEADING
                    ),),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_PENDING,style: TextStyle(
                        fontFamily: Strings.FONT_FAMILY_HEADING
                    ),),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              ReservationsAll(),
              ReservationPending()
            ],
          ),
        )
    );
  }
}

