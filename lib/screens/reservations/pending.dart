import 'package:flutter/material.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/models/reservation.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/screens/reservations/form.dart';
import 'package:hotel_project/models/room.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/common/strings.dart';

class ReservationPending extends StatefulWidget {
  const ReservationPending({Key? key}) : super(key: key);

  @override
  _ReservationPendingState createState() => _ReservationPendingState();
}

class _ReservationPendingState extends State<ReservationPending> {
  List<Reservation> reservationList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    reservationList = [
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Azmat Karim",username: "saharakks",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_NEW),
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Aqeel",username: "aqeel",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_CONFIRMED),
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Tariq",username: "tariq",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_REJECTED),
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Gari Khan",username: "gari",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_CANCELED)
    ];
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: Container(
          child: ListView.builder(
              itemCount: reservationList.length,
              itemBuilder: (BuildContext context, int index) {
                Reservation currentReservation = reservationList[index];
                return Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, right: 10),
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, left: 10, right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 7,
                                  child: Align(
                                    // alignment: Alignment.centerLeft,
                                    child: Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            currentReservation.customer.name,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: Strings.FONT_FAMILY_HEADING,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            currentReservation.remarks,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: Strings.FONT_FAMILY_TEXT,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Row(
                                      children: [
                                        // Container(
                                        //   height: 50,
                                        //   width: 1,
                                        //   color: Colors.grey,
                                        // ),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        IconButton(
                                            onPressed: () {
                                              Util.log('Button', 'Arrow Clicked');
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ReservationForm()));
                                            },
                                            icon: Icon(
                                              Icons.double_arrow,
                                              size: 30,
                                            )),
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ),
      ),
    );
  }
}
