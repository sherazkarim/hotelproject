import 'package:flutter/material.dart';
import 'package:hotel_project/models/customer.dart';
import 'package:hotel_project/models/reservation.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/common/strings.dart';

class ReservationsAll extends StatefulWidget {
  const ReservationsAll({Key? key}) : super(key: key);

  @override
  _ReservationsAllState createState() => _ReservationsAllState();
}

class _ReservationsAllState extends State<ReservationsAll> {
  List<Reservation> reservationList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    reservationList = [
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Azmat Karim",username: "saharakks",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_NEW),
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Aqeel",username: "aqeel",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_CONFIRMED),
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Tariq",username: "tariq",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_REJECTED),
      Reservation(id: -1,
          customer: Customer(id: -1,name: "Gari Khan",username: "gari",email: "email@email.com",cnic: "999",address: "Zulfiqarabad"),
          customerId: -1, date: "16-Feb-2022", time: "11:00", durationOfStaysDays: 3, durationOfStaysHours: 0, adults: 2, children: 1,
          deposit: 5000, vehicleNo: "GLT-4588", reference: "self" ,remarks : "Reservation Confirmed",status: Reservation.STATUS_CANCELED)
    ];
  }
  Future<void> _showDialog() async{
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
          contentPadding: EdgeInsets.all(10),
          title: Center(
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    Text(
                      'Are you sure???',
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black),
                    ),
                    SizedBox(height: 30,),
                  ],
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(           // FlatButton widget is used to make a text to work like a button
              textColor: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },        // function used to perform after pressing the button
              child: Text('Yes'),
            ),
            FlatButton(
              textColor: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('No'),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Scaffold(
      body:Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
          child: SafeArea(
            child: Container(
              child: ListView.builder(
                  itemCount: reservationList.length,
                  itemBuilder: (BuildContext context, int index) {

                    Reservation currentReservation = reservationList[index];

                    return Padding(
                      padding: const EdgeInsets.only(top: 0.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0, right: 10),
                            child: Card(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 10, right: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 7,
                                      child: Align(
                                        // alignment: Alignment.centerLeft,
                                        child: Column(
                                          children: [
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                currentReservation.customer.name,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: Strings.FONT_FAMILY_HEADING,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                currentReservation.remarks,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Row(
                                        children: [
                                          // Container(
                                          //   height: 50,
                                          //   width: 1,
                                          //   color: Colors.grey,
                                          // ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Column(
                                            children: [
                                              ElevatedButton(onPressed:(){
                                                _showDialog();
                                              },
                                                style: ButtonStyle(
                                                    backgroundColor: MaterialStateProperty.all(Color(0xFF939ea5)),
                                                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                        RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(18),
                                                            side: BorderSide(color: Colors.grey)
                                                        )
                                                    )
                                                ),
                                                child: Text(Strings.TEXT_CANCEL,style: TextStyle(
                                                    fontSize: 13
                                                ),),),
                                              Text(
                                                currentReservation.getStatusText(),
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: Strings.FONT_FAMILY_TEXT,
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }),
            ),
          ),
      ),
    );
  }
}
