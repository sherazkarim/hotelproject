import 'package:flutter/material.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/common/strings.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  Future<void> _showDialog() async{
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
          contentPadding: EdgeInsets.all(10),
          title:Text(Strings.TEXT_RESET_SUCCESS_MSG,style: TextStyle(
            fontFamily: Strings.FONT_FAMILY_TEXT,
            fontSize: 15
          ),),
          actions: [
            FlatButton(           // FlatButton widget is used to make a text to work like a button
              textColor: Colors.black,
              onPressed: () {
                Navigator.pop(context);
              },        // function used to perform after pressing the button
              child: Text('ok',style: TextStyle(
                  fontFamily: Strings.FONT_FAMILY_HEADING,
                  fontSize: 15
              ),),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // SizedBox(height: 80,),
              Container(
                height: 150,
                width: 200,
                child: Image.asset(Strings.IMG_LOGO),
              ),
              SizedBox(
                height: 20,
              ),
              Text(Strings.TEXT_RESET_MSG,
                  style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
              SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 40),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: Strings.TEXT_HINT_MSG,
                      labelText: User.LABEL_EMAIL,
                      labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                ),
              ),

              SizedBox(height: size.height * 0.03),
              RaisedButton(
                onPressed: () {

                  print("Reset Button Clicked");
                  _showDialog();
                  // Navigator.push(context, MaterialPageRoute(builder: (context)=>()));
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                textColor: Colors.white,
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: size.width * 0.5,
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(80.0),
                      gradient: new LinearGradient(
                          colors: [Color(0xFF095269), Color(0xFF44b2c9)])),
                  // padding: const EdgeInsets.all(0),
                  child: GestureDetector(
                    child: Text(
                      Strings.TEXT_SEND,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: Strings.FONT_FAMILY_TEXT,
                          fontSize: 16),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
