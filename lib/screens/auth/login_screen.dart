import 'package:flutter/material.dart';
import 'package:hotel_project/models/user.dart';
import 'package:hotel_project/screens/dashboard.dart';
import 'package:hotel_project/screens/auth/forget_password.dart';
import 'package:hotel_project/common/strings.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // SizedBox(height: 80,),
              Container(
                height: 150,
                width: 200,
                child: Image.asset(Strings.IMG_LOGO),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0),
                child: Form(
                    key: _formkey,
                    child: Column(
                      children: [
                        TextFormField(
                          initialValue: "sheraz@gmail.com",
                          decoration: InputDecoration(
                              labelText: User.LABEL_EMAIL,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                          maxLength: 50,
                        ),
                        TextFormField(
                          initialValue: "sheraz123",
                          decoration: InputDecoration(
                              labelText:  User.LABEL_PASSWORD,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                          maxLength: 22,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                      ],
                    )),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgotPassword()));
                  },
                  child: Text(
                    Strings.ERROR_FORGET_PASSWORD,
                    style: TextStyle(
                        fontSize: 12,
                        // color: Color(0xFF02bbd7),
                        fontFamily: Strings.FONT_FAMILY_TEXT),
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.05),
              RaisedButton(
                onPressed: () {
                  if (_formkey.currentState!.validate()) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DashboardScreen()),
                    );
                  }
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                textColor: Colors.white,
                padding: const EdgeInsets.all(0),
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: size.width * 0.5,
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(80.0),
                      gradient: new LinearGradient(
                          colors: [Color(0xFF095269), Color(0xFF44b2c9)])),
                  // padding: const EdgeInsets.all(0),
                  child: GestureDetector(
                    child: Text(
                      Strings.TEXT_LOGIN,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: Strings.FONT_FAMILY_TEXT,
                          fontSize: 16),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
