import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/models/user.dart';
import 'package:intl/intl.dart';

class BillingEntry extends StatefulWidget {
  const BillingEntry({Key? key}) : super(key: key);

  @override
  _BillingEntryState createState() => _BillingEntryState();
}

class _BillingEntryState extends State<BillingEntry> {
  final _formkey = GlobalKey<FormState>();
  TextEditingController dateinput = TextEditingController();
  TextEditingController timeinput = TextEditingController();
  TimeOfDay selectedTime = TimeOfDay.now();

  @override
  void initState() {
    dateinput.text = ""; //set the initial value of text field
    super.initState();
  }

  String? selectroom;
  String defalutroom = "Room 101";
  List<String> roomsList = [
    'Room 101',
    'Room 102',
    'Room 103',
    'Room 104',
  ];

  String? billedto;
  String dbilledto = "Self";
  List<String> billedList = [
    'Self',
    'Brother',
    'Sister',
    'Father',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Strings.IMG_BG),
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            Strings.TEXT_CHOOSE_ROOM,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Strings.FONT_FAMILY_HEADING),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      defalutroom,
                                      style: TextStyle(
                                          fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: roomsList
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0),
                                            child: Text(
                                              item,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily:
                                                      Strings.FONT_FAMILY_TEXT),
                                            ),
                                          )))
                                      .toList(),
                                  value: selectroom,
                                  onChanged: (value) {
                                    setState(() {
                                      selectroom = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text(
                            Strings.TEXT_BILLED_TO,
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: Strings.FONT_FAMILY_HEADING),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 10,
                            child: Container(
                              height: 35,
                              width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.grey)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton2(
                                  hint: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      dbilledto,
                                      style: TextStyle(
                                          fontFamily: Strings.FONT_FAMILY_TEXT),
                                    ),
                                  ),
                                  items: billedList
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0),
                                            child: Text(
                                              item,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily:
                                                      Strings.FONT_FAMILY_TEXT),
                                            ),
                                          )))
                                      .toList(),
                                  value: billedto,
                                  onChanged: (value) {
                                    setState(() {
                                      billedto = value as String;
                                    });
                                  },
                                  buttonHeight: 10,
                                  buttonWidth: 110,
                                  itemHeight: 40,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                          child: TextFormField(
                        controller: dateinput,
                        decoration: InputDecoration(
                            suffixIcon: Icon(Icons.calendar_today),
                            // icon: Icon(Icons.calendar_today),
                            labelText: User.LABEL_CHECK_OUT_DATE),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return Strings.ERROR_EMPTY;
                          }
                          return null;
                        },
                        readOnly: true,
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              //DateTime.now() - not to allow to choose before today.
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            print(
                                pickedDate); //pickedDate output format => 2021-03-10 00:00:00.000
                            String formattedDate =
                                DateFormat('yyyy-MM-dd').format(pickedDate);
                            print(
                                formattedDate); //formatted date output using intl package =>  2021-03-16
                            //you can implement different kind of Date Format here according to your requirement

                            setState(() {
                              dateinput.text =
                                  formattedDate; //set output date to TextFormField value.
                            });
                          } else {
                            Strings.ERROR_EMPTY;
                          }
                        },
                      )),
                      Container(
                          alignment: Alignment.center,
                          child: Center(
                              child: TextFormField(
                            controller: timeinput,
                            //editing controller of this TextFormField
                            decoration: InputDecoration(
                                suffixIcon: Icon(Icons.timer),
                                labelText: Strings
                                    .TEXT_CHECK_OUT_TIME //label text of field
                                ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return Strings.ERROR_EMPTY;
                              }
                              return null;
                            },
                            readOnly: true,
                            //set it true, so that user will not able to edit text
                            onTap: () async {
                              TimeOfDay? pickedTime = await showTimePicker(
                                initialTime: TimeOfDay.now(),
                                context: context,
                              );

                              if (pickedTime != null) {
                                print(pickedTime
                                    .format(context)); //output 10:51 PM
                                DateTime parsedTime = DateFormat.jm().parse(
                                    pickedTime.format(context).toString());
                                //converting to DateTime so that we can further format on different pattern.
                                print(
                                    parsedTime); //output 1970-01-01 22:53:00.000
                                String formattedTime =
                                    DateFormat('HH:mm:ss').format(parsedTime);
                                print(formattedTime); //output 14:59:00
                                //DateFormat() is from intl package, you can format the time on any pattern you need.

                                setState(() {
                                  timeinput.text =
                                      formattedTime; //set the value of text field.
                                });
                              } else {
                                Strings.ERROR_EMPTY;
                              }
                            },
                          ))),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_NAME,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_NUMBER,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_EMAIL,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_POSTAL_CODE,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_CITY,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_PAYEE_STATE,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_PAYEE_COUNTRY,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: TextFormField(
                          decoration: InputDecoration(
                              labelText: User.LABEL_ADDRESS,
                              labelStyle: TextStyle(
                                  fontFamily: Strings.FONT_FAMILY_TEXT)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return Strings.ERROR_EMPTY;
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formkey.currentState!.validate()) {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //       builder: (context) => CheckInDetails()),
                                // );
                              }
                              // _showDailoge();
                              Util.log('Button', 'Save Details');
                            },
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Color(0xFF939ea5)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(18),
                                        side: BorderSide(color: Colors.grey)))),
                            child: Text(
                              Strings.TEXT_SAVE_DETAILS,
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: Strings.FONT_FAMILY_TEXT,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
