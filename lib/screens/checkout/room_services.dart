import 'package:flutter/material.dart';
import 'package:hotel_project/common/strings.dart';

class RoomServices extends StatefulWidget {
  const RoomServices({Key? key}) : super(key: key);

  @override
  _RoomServicesState createState() => _RoomServicesState();
}

class _RoomServicesState extends State<RoomServices> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10,bottom: 10),
              child: Container(
                height:double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SERIAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_CHECK_IN_DATE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "2022-08-08",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_CHECK_IN_TIME,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "19:25",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_CHECK_OUT_DATE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "2022-03-09",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_CHECK_OUT_TIME,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "23:35",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_FARE_DAY,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "0.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DURATION,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "10 Days",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "0",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.0.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SERIAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_CHECK_IN_DATE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "2022-08-08",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_CHECK_IN_TIME,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "19:25",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_CHECK_OUT_DATE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "2022-03-09",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_CHECK_OUT_TIME,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "23:35",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_FARE_DAY,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "0.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DURATION,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "10 Days",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "0",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.0.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
