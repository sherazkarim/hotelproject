import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:hotel_project/common/strings.dart';
import 'package:hotel_project/common/util.dart';
import 'package:hotel_project/models/user.dart';

class FinalizeBilling extends StatefulWidget {
  const FinalizeBilling({Key? key}) : super(key: key);

  @override
  _FinalizeBillingState createState() => _FinalizeBillingState();
}

class _FinalizeBillingState extends State<FinalizeBilling> {

  String? taxType;
  String defaluTax = "N/A";
  List<String> taxlist = [
    'N/A',
    'Damage utensils',
    'Damage bed',
    'Damage chair',
  ];

  String? paymentMode;
  String defalutpayment = "Cash";
  List<String> paymentBy = [
    'Check',
    'Balance',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10,bottom: 10),
              child: Container(
                height:double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_ROOM_CHARGES,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "0.00 PKR",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_FOOD_CHARGES,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "195.0 PKR",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TAX,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "0.00 PKR",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_DEPOSIT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING
                                    ),
                                  ),
                                  Text(
                                    "1000 PKR",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL_ABC,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "195.0 PKR",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_TOTAL_ABCD,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "800.0",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                children: [
                                  Text(
                                    Strings.TEXT_TAX_TYPE,
                                    style: TextStyle(fontSize: 14, fontFamily: Strings.FONT_FAMILY_HEADING),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 10,
                                    child: Container(
                                      height: 35,
                                      width: 200,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                          border: Border.all(color: Colors.grey)),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton2(
                                          hint: Padding(
                                            padding: const EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              defaluTax,
                                              style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                            ),
                                          ),
                                          items: taxlist
                                              .map((item) => DropdownMenuItem<String>(
                                              value: item,
                                              child: Padding(
                                                padding:
                                                const EdgeInsets.only(left: 10.0),
                                                child: Text(
                                                  item,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: Strings.FONT_FAMILY_TEXT),
                                                ),
                                              )))
                                              .toList(),
                                          value: taxType,
                                          onChanged: (value) {
                                            setState(() {
                                              taxType = value as String;
                                            });
                                          },
                                          buttonHeight: 10,
                                          buttonWidth: 110,
                                          itemHeight: 40,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: TextField(
                                  decoration: InputDecoration(
                                      labelText: User.LABEL_TAX_AMOUNT,
                                      labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                                  obscureText: true,
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Row(
                                children: [
                                  Text(
                                    Strings.TEXT_PAYMENT_MODE,
                                    style: TextStyle(fontSize: 14, fontFamily: Strings.FONT_FAMILY_HEADING),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 10,
                                    child: Container(
                                      height: 35,
                                      width: 200,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                          border: Border.all(color: Colors.grey)),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton2(
                                          hint: Padding(
                                            padding: const EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              defalutpayment,
                                              style: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT),
                                            ),
                                          ),
                                          items: paymentBy
                                              .map((item) => DropdownMenuItem<String>(
                                              value: item,
                                              child: Padding(
                                                padding:
                                                const EdgeInsets.only(left: 10.0),
                                                child: Text(
                                                  item,
                                                  style: TextStyle(
                                                      fontSize: 14,
                                                      fontFamily: Strings.FONT_FAMILY_TEXT),
                                                ),
                                              )))
                                              .toList(),
                                          value: paymentMode,
                                          onChanged: (value) {
                                            setState(() {
                                              paymentMode = value as String;
                                            });
                                          },
                                          buttonHeight: 10,
                                          buttonWidth: 110,
                                          itemHeight: 40,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: TextField(
                                  decoration: InputDecoration(
                                      labelText: User.LABEL_AMOUNT_PAID,
                                      labelStyle: TextStyle(fontFamily: Strings.FONT_FAMILY_TEXT)),
                                  obscureText: true,
                                ),
                              ),
                              SizedBox(height: 15,),
                              Container(
                                height: 50,
                                width: double.infinity,
                                child: TextField(
                                  obscureText: false,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.zero
                                    ),
                                    hintText: Strings.TEXT_ADDITIONAL_NOTES,
                                  ),
                                ),
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Return : 804",style: TextStyle(
                                    fontFamily: Strings.FONT_FAMILY_HEADING,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18
                                  ),),
                                  ElevatedButton(
                                    onPressed: () {
                                      // _showDailoge();
                                      Util.log('Button', 'Complete Check Out');
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                        MaterialStateProperty.all(Color(0xFF939ea5)),
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(18),
                                                side: BorderSide(color: Colors.grey)))),
                                    child: Text(
                                      Strings.TEXT_COMPLETE_CHECK_OUT,
                                      style: TextStyle(
                                        fontSize: 15,
                                        fontFamily: Strings.FONT_FAMILY_TEXT,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10,),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
