import 'package:flutter/material.dart';
import 'package:hotel_project/screens/checkOut/billing_entry.dart';
import 'package:hotel_project/screens/checkOut/finalize.dart';
import 'package:hotel_project/screens/checkOut/food_services.dart';
import 'package:hotel_project/screens/checkOut/room_services.dart';
import 'package:hotel_project/common/strings.dart';

class BillingScreen extends StatefulWidget {
  const BillingScreen({Key? key}) : super(key: key);

  @override
  _BillingScreenState createState() => _BillingScreenState();
}

class _BillingScreenState extends State<BillingScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text(Strings.TEXT_CHECK_OUT,style: TextStyle(
              fontFamily: Strings.FONT_FAMILY_HEADING,
            ),),
            backgroundColor: Color(0xFF02bbd7),
            elevation: 0,
            bottom: TabBar(
              unselectedLabelColor:Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Strings.GREY_COLOR,
                      Strings.GREY_COLOR
                    ]),
                borderRadius: BorderRadius.circular(5),),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_BILLING_ENTRY),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_ROOM_SERVICES),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_FOOD_SERVICES),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_FINALIZE),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              BillingEntry(),
              RoomServices(),
              FoodServices(),
              FinalizeBilling(),
            ],
          ),
        )
    );
  }
}
