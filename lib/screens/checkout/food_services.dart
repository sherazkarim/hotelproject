import 'package:flutter/material.dart';
import 'package:hotel_project/common/strings.dart';

class FoodServices extends StatefulWidget {
  const FoodServices({Key? key}) : super(key: key);

  @override
  _FoodServicesState createState() => _FoodServicesState();
}

class _FoodServicesState extends State<FoodServices> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Strings.IMG_BG),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10,bottom: 10),
              child: Container(
                height:double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SERIAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_ITEM,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Pepsi/1 Litre",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_PRICE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "75",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_QUANTITY,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DISOUNT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "0.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "75.0",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children:[
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.150.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10,top: 15),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SERIAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_ITEM,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "Pepsi/1 Litre",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_PRICE,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "75",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    Strings.TEXT_QUANTITY,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_DISOUNT,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                  Text(
                                    "0.00",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_TEXT,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    Strings.TEXT_SUB_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "75.0",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children:[
                                  Text(
                                    Strings.TEXT_TOTAL,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                  Text(
                                    "Rs.150.00/-",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontFamily: Strings.FONT_FAMILY_HEADING,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 15,),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
