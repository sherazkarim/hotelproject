import 'package:flutter/material.dart';
import 'package:hotel_project/screens/checkOut/finalize.dart';
import 'package:hotel_project/screens/checkOut/food_services.dart';
import 'package:hotel_project/common/strings.dart';

class ServiceOrderScreen extends StatefulWidget {
  const ServiceOrderScreen({Key? key}) : super(key: key);

  @override
  _ServiceOrderScreenState createState() => _ServiceOrderScreenState();
}

class _ServiceOrderScreenState extends State<ServiceOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text(Strings.TITLE_SERVICE_ORDER,style: TextStyle(
              fontFamily: Strings.FONT_FAMILY_HEADING,
            ),),
            backgroundColor: Color(0xFF02bbd7),
            elevation: 0,
            bottom: TabBar(
              unselectedLabelColor:Colors.white,
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xFF939ea5),
                      Color(0xFF939ea5)
                    ]),
                borderRadius: BorderRadius.circular(5),),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_ALL),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(Strings.TEXT_ROOMS),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
            FinalizeBilling(),
              FoodServices()
            ],
          ),
        )
    );
  }
}
